<!DOCTYPE html>
<html>
    <head>
        <title>Single</title>
        <meta http-equiv="Content-Type" content="text/html; Charset=UTF-8"> 
        <script src="JS/jquery-3.2.1.min.js" type="text/javascript"></script>
        <script src="JS/myscripts.js" type="text/javascript"></script>
        <link href="Style/error_style.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css"> 
    </head>

    <?php
        require_once 'Config.php';
        require_once 'Connection.php';
        require 'Manager.php';
        
        error_reporting(E_ALL);
        ini_set('dispaly_error',1);
        
        function errorHandler($level, $mess, $file, $line, $context){
            switch($level){
                case E_WARNING:
                    $type = "Warning";
                    break;
                case E_NOTICE:
                    $type = "Notice";
                    break;
                case E_STRICT:
                    $type = "Strict";
                    break;
                default;
                    return false;
            }
            
            if ($level) {
                ManagerClass::writeErrorToLog($type, $mess, $line);
            } 
            return true;
        }
        set_error_handler('errorHandler', E_ALL);
    
        $db_conn = new ConnectionClass(SERVER, USER_NAME, PASSWORD ,DATABASE);
        $conn = $db_conn->connect_db();
        if ($conn) {
    ?>

    <body>
        <div class="container">
            <h2>SPA web store</h2>
                <div id="preview"></div>
                <form class="form-inline" method="post" enctype='multipart/form-data' id="myform" name="myform">
           
                    <div id="content"></div>
                    <input type="hidden" name="MAX_FILE_SIZE" value="30000" />
            
                    <div class="form-group">
                        <input class="btn btn-default"  type="file" id="myfile" name="myfile" accept=".csv" />
                    </div>
                
                    <div class="form-group">
                        <input class="btn btn-default" type="submit" name="send" id="send" value="Submit" />    
                    </div>
                </form>    
        </div>
    </body> 
    
    <?php
    }
    ?>
</html>
