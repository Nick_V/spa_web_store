$(document).ready(function(){
       
    function readFile ( input ) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
 
      reader.onload = function (e) {
        var table = $("<table />");
        var rows = e.target.result.split("\n");
        
          for(var i = 0; i < rows.length; i++){
            var row = $("<tr />");
            var cells = rows[i].split(",");
            for(var j = 0; j < cells.length; j++){
                var cell = $("<td />");
                cell.html(cells[j]);
                row.append(cell);
            }
            table.append(row);
        }
        $('#preview').html('');
        $('#preview').append(table);
        
      };
 
      reader.readAsText($("#myfile")[0].files[0]);
    }
  }
  
  $('#myfile').change(function(){
    readFile(this);
  });
  
     
    $('#myform').on('submit', function(e){
        e.preventDefault();
  
        var mydata = new FormData(this);
       
        $.ajax({
            type: 'POST',
            url: 'handler.php',
            data: mydata,
            cache: false,
            processData: false,
            contentType: false,
            success: function(data){
                $('#content').html(data);
            },
            error: function(data){
                alert('error ajax');
           }
            
        });
        
    });
    
    
    
});
    



