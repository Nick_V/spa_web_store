<?php
    require 'IManager.php';
    
    class ManagerClass implements IManager
    {
        public function getAllProducts($conn)
        {

//  Examlpe #1
//           
//            $sql_query = "SELECT DISTINCT product.product_name, product.prod_id FROM product INNER JOIN store ON product.prod_id = store.prod_id ";
//            $result = $conn->prepare($sql_query);
//            $result->execute();
//            
//            if ($result->rowCount() > 0) {
//                
//                echo "<table class='table table-bordered'><tr><th>ProductName" . "</th><th>Qty" . "</th><th>Warehouse</th></tr>";
//                while($row = $result->fetch()){
//                    echo "<tr><td>" . $row["product_name"] . "</td>"
//                            . "<td>" . $this->getCountProducts($conn, $row["prod_id"]) . "</td>"
//                            . "<td>" . $this->getStores($conn, $row["prod_id"]) . "</td></tr>";
//                }
//                echo "</table>";
//            }
//            else {
//                echo 'Not result' . "</br>";
//            }
            
//  Example #2            
            $sql_query = "SELECT product.*, SUM(store.qty) AS qty, GROUP_CONCAT(warehouse.wh_name) AS wh_name FROM product "
                    . "LEFT JOIN store ON product.prod_id = store.prod_id "
                    . "LEFT JOIN warehouse ON store.wh_id = warehouse.wh_id WHERE product.prod_id = store.prod_id GROUP BY prod_id";
            $result = $conn->prepare($sql_query);
            $result->execute();
            
            if ($result->rowCount() > 0) {
                
                echo "<table class='table table-bordered'><tr><th>ProductName" . "</th><th>Qty" . "</th><th>Warehouse</th></tr>";
                while($row = $result->fetch()){
                    echo "<tr><td>" .  $row["product_name"] . "</td>"
                            . "<td>" . $row["qty"] . "</td>"
                            . "<td>" . $row["wh_name"] . "</td></tr>";
                }
                echo "</table>";
            }
            else {
                echo 'Not result' . "</br>";
            }
            
        }
        
        public function getProductName($conn, $id)
        {
            $sql_products = "SELECT product_name FROM product WHERE prod_id = $id";
            $result = $conn->prepare($sql_products);
            $result->execute();
            
            if ($result->rowCount() > 0) {
                
               while($row = $result->fetch()){
                    return $row["product_name"];
               } 
            }
        }
        
        public function getStoreName($conn, $id) 
        {
            $sql_query = "SELECT wh_name FROM warehouse WHERE wh_id = '$id'";
            $result = $conn->prepare($sql_query);
            $result->execute();
            
            if ($result->rowCount() > 0) {
                while($row = $result->fetch()) {
                    return $row["wh_name"];
                }
            }
        }
        
        public function getProductId($conn, $name)
        {
            $sql_query = "SELECT prod_id FROM product WHERE product_name = '$name'";
            $result = $conn->prepare($sql_query);
            $result->execute(); 
            
            if ($result->rowCount() > 0) {
                while($row = $result->fetch()) {
                    return $row["prod_id"];
                }
            } 
        }
        
        public function getWarehouseId($conn, $name)
        {
            $sql_query = "SELECT wh_id FROM warehouse WHERE wh_name = '$name'";
            $result = $conn->prepare($sql_query);
            $result->execute();
            
            if ($result->rowCount() > 0) {
                while($row = $result->fetch()) {
                    return $row["wh_id"];
                }
            }
        }
        
        public function getCountProducts($conn, $prod_id) 
        {
            $sql_query = "SELECT qty FROM store WHERE prod_id LIKE '$prod_id'";
            $result = $conn->prepare($sql_query);
            $result->execute();
            $count = 0;
            if ($result->rowCount() > 0) {
                while ($row = $result->fetch()){
                    $count += $row["qty"];
                }
                return $count;
            }
            else {
                echo 'Not result' . "</br>";
            }
        }

        public function getStores($conn, $prod_id) 
        {
            $sql_query = "SELECT wh_id FROM store WHERE prod_id LIKE '$prod_id'";
            $str = "";
            $result = $conn->prepare($sql_query);
            $result->execute();
            
            if ($result->rowCount() > 0) {
                while($row = $result->fetch()) {
                    if ($str != "") {
                        $str = $str . ", " . $this->getStoreName($conn, $row["wh_id"]);
                    }
                    else if ($str == "") {   
                        $str = $str . $this->getStoreName($conn, $row["wh_id"]);
                    }
                }
                return $str;
            }
            else {                
                echo 'Not result'."</br>";            
            }
        }
        
        public function updateProducts($conn, $handle)
        {
            while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                               
                if (($this->isProductSet($conn, $data)) && ($this->isWarehouseSet($conn, $data))) {
                    
                    $id_pr = $this->getProductId($conn, $data[0]);    
                    $id_wh = $this->getWarehouseId($conn, $data[2]);
                    
                    $sql_select = "SELECT qty FROM store WHERE prod_id = '$id_pr' AND wh_id = '$id_wh'";
                    $result = $conn->prepare($sql_select);
                    $result->execute();
                    
                    if ($result->rowCount() > 0) {
                        
                        while ($row = $result->fetch()) {
                        $old_qty = $row["qty"];
                        } 
                        
                        $new_qty = (int)$old_qty + (int)$data[1];
                        
                        $this->isProductEmpty($new_qty, $id_pr, $id_wh, $conn);
                        $sql_update = "UPDATE store SET qty = '$new_qty' WHERE prod_id = '$id_pr' AND wh_id = '$id_wh'";
                        $conn->exec($sql_update);
                        
                    }
                    else {
                        $this->insertIntoStore($conn, $data, $id_pr, $id_wh);
                    }
                }
                else if ((!$this->isProductSet($conn, $data)) 
                        && $data[0] != "" && $data[1] > 0 && $data[2] != "" 
                        && (!$this->isWarehouseSet($conn, $data))) {
                                        
                    $this->createNewProduct($conn, $data);
                    $new_prod_id = $this->getProductId($conn, $data[0]);
                    
                    $this->createNewWarehouse($conn, $data);
                    $new_wh_id = $this->getWarehouseId($conn, $data[2]);
                    
                    $this->insertIntoStore($conn, $data, $new_prod_id, $new_wh_id);
                }
                else if ((!$this->isProductSet($conn, $data)) 
                        && $data[0] != "" && $data[1] > 0 && $data[2] != "" 
                        && ($this->isWarehouseSet($conn, $data)) ) {
                    
                    $this->createNewProduct($conn, $data);
                    $new_prod_id = $this->getProductId($conn, $data[0]);
                    
                    $id_wh = $this->getWarehouseId($conn, $data[2]);
                     
                    $this->insertIntoStore($conn, $data, $new_prod_id, $id_wh);
                }
                else if (($this->isProductSet($conn, $data)) 
                        && $data[0] != "" && $data[1] > 0 && $data[2] != "" 
                        && (!$this->isWarehouseSet($conn, $data))) {
                    
                    $this->createNewWarehouse($conn, $data);
                    $new_wh_id = $this->getWarehouseId($conn, $data[2]);
                    
                    $id_pr = $this->getProductId($conn, $data[0]);
                    
                    $this->insertIntoStore($conn, $data, $id_pr, $new_wh_id);
                }
               
            }
        }
        
        public function initialise($conn, $handle) 
        { 
            $counter = 0;
            while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                
               
                if ($counter == 0 && $data[0] != "" && $data[1] > 0 && $data[2] != "") {
                    $this->createNewProduct($conn, $data);
                    $this->createNewWarehouse($conn, $data);
                    
                    $prod_id = $this->getProductId($conn, $data[0]);
                    $wh_id = $this->getWarehouseId($conn, $data[2]);
                    
                    $this->insertIntoStore($conn, $data, $prod_id, $wh_id);
                }
                else {
                    
                    if (!$this->isProductSet($conn, $data)){
                        $this->createNewProduct($conn, $data); 
                        $prod_id = $this->getProductId($conn, $data[0]);
                    }
                    else {
                        $prod_id = $this->getProductId($conn, $data[0]);
                    }
                
                    if (!$this->isWarehouseSet($conn, $data)) {
                        $this->createNewWarehouse($conn, $data);
                        $wh_id = $this->getWarehouseId($conn, $data[2]);
                    }
                    else {
                        $wh_id = $this->getWarehouseId($conn, $data[2]);
                    }
                
                    if (!$this->isStoreSet($conn)) {
                        $this->insertIntoStore($conn, $data, $prod_id, $wh_id);
                    }
                    else {
                        $sql_select = "SELECT prod_id, wh_id, qty FROM store WHERE prod_id = '$prod_id' AND wh_id = '$wh_id'";
                        $result = $conn->prepare($sql_select);
                        $result->execute();
                        if ($result->rowCount() > 0) {
                            while ($row = $result->fetch_assoc()) {
                                $old_qty = $row["qty"];
                            }
                            $new_qty = (int)$old_qty + (int)$data[1];
                            $this->isProductEmpty($new_qty, $prod_id, $wh_id, $conn);
                            $sql_update = "UPDATE store SET qty = '$new_qty' WHERE prod_id = '$prod_id' AND wh_id = '$wh_id'";
                            $conn->exec($sql_update);
                        }
                        else {
                            $this->insertIntoStore($conn, $data, $prod_id, $wh_id);
                        }
                    }
                }
                $counter++;
            }
        }
        
        public function isProductSet($conn, $data) 
        {
            $sql = "SELECT prod_id FROM product WHERE product_name = '$data[0]'";
            $result = $conn->prepare($sql);
            $result->execute();
            
            if ($result->rowCount() == 0) { 
                return false;
            }
            else {
                return true;
            }
        }
        
        public function isWarehouseSet($conn, $data) 
        {
            $sql = "SELECT wh_id FROM warehouse WHERE wh_name = '$data[2]'";
            $result = $conn->prepare($sql);
            $result->execute();
            
            if ($result->rowCount() == 0) { 
                return false;
            }
            else {
                return true;
            }    
        }
        
        public function isStoreSet($conn) 
        {
            $sql = "SELECT * FROM store";
            $result = $conn->prepare($sql);
            $result->execute();
            if ($result->rowCount() > 0) {
                return false;
            }
            else {
                return true;
            }
        }
        
        public function createNewProduct($conn, $data) 
        {
            $sql_insert = "INSERT INTO product(product_name) VALUES ('$data[0]')";
            $conn->exec($sql_insert);
        }
        
        public function createNewWarehouse($conn, $data) 
        {
            $sql_insert = "INSERT INTO warehouse(wh_name) VALUES ('$data[2]')";
            $conn->exec($sql_insert);
        }
        
        public function insertIntoStore($conn, $data, $new_prod_id, $new_wh_id)
        {
            if ($data[1] > 0) {
                $sql_insert_store = "INSERT INTO store (prod_id, wh_id, qty) VALUES ('$new_prod_id','$new_wh_id','$data[1]')";
                $conn->exec($sql_insert_store);    
            }
        }        
        
        public function isProductEmpty($new_qty, $id_product, $id_wh, $conn)
        {
            if ($new_qty <= 0) {
                $this->clearProductInStore($conn, $id_product, $id_wh);
            }
        }
        
        public function clearProductInStore($conn, $id_product, $id_wh)
        {
            $sql_query_delete = "DELETE FROM store WHERE prod_id = '$id_product' AND wh_id ='$id_wh'";
            $conn->exec($sql_query_delete);
        }
        
        public static function writeErrorToLog($type, $mess, $line) 
        {
            $file = fopen('errorLog.log', 'a');
            
                if (!empty($file)) {
                    $err = "Type: $type,\n" 
                        . "Message: $mess\n" 
                        . "Line: $line\n" 
                        . "Date: ". date('Y-m-d H:i:s'). "\n" . "---" . "\n\n";
                    fwrite($file, $err); 
                    fclose($file);
            }        
        }
}

