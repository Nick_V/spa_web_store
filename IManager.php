<?php

interface IManager
{
    public function createNewProduct($conn, $data);
    public function createNewWarehouse($conn, $data);
    public function getAllProducts($conn);
    public function updateProducts($conn, $handle);
    public function clearProductInStore($conn, $id_product, $id_wh);
}
