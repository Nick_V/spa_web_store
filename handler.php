<?php
    require_once 'Config.php';
    require_once 'Connection.php';
    require 'Manager.php';
           
    $db_conn = new ConnectionClass(SERVER, USER_NAME, PASSWORD , DATABASE);
    $conn = $db_conn->connect_db();
    $sql_is_product_empty = "SELECT * FROM product";
    $sql_is_wh_empty = "SELECT * FROM warehouse";
    $sql_is_store_empty  = "SELECT * FROM store";
            
    $manager = new ManagerClass();
           
    if (isset($_FILES) && $_FILES['myfile']['size'] > 0) {
    
        $file = $_FILES['myfile']['tmp_name'];
        $handle = fopen($file, "r");
        
        $stores = $conn->prepare($sql_is_store_empty);
        $products = $conn->prepare($sql_is_product_empty);
        $whs = $conn->prepare($sql_is_wh_empty);
        
        $stores->execute();
        $products->execute();
        $whs->execute();
        
        if ($stores->rowCount() == 0 && $products->rowCount() == 0 && $whs->rowCount() == 0) {
                        
            $manager->initialise($conn, $handle);
            $manager->getAllProducts($conn);
        }
        else {
                        
            $manager->updateProducts($conn, $handle);
            $manager->getAllProducts($conn);
        }
    }
    else {
        
        $manager->getAllProducts($conn); 
    } 
            
        

