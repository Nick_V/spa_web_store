<?php

class ConnectionClass
{
    public $server_name = '';
    public $user_name = '';
    public $password = '';
    public $db_name = '';
    
    
    function __construct($server_name, $user_name, $password, $db_name) 
    {
        $this->server_name = $server_name;
        $this->user_name = $user_name;
        $this->password = $password;
        $this->db_name = $db_name;
    }

    public function connect_db()
    {   
        $dsn = "mysql:host=$this->server_name; dbname=$this->db_name";
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];
        
        try {
            $connection = new PDO($dsn, $this->user_name, $this->password, $options);
            return $connection;
        } catch (PDOException $e) {
            include 'Error.php';
        }
    }
}


